import 'package:intl/intl.dart';

class Constants {
  static final DateFormat defaultDisplayDateFormat = DateFormat.yMMMEd();

  // standard app margins
  static const double smallMargin = 5.0;
  static const double mediumMargin = 10.0;
  static const double largeMargin = 15.0;

  // standard app padding
  static const double smallPadding = 5.0;
  static const double mediumPadding = 10.0;
  static const double largePadding = 20.0;
}

const columnId = "id";
const columnInfo = "info";
const columnCreatedBy = "c_by";
const columnUpdatedBy = "c_by";
const columnCreatedAt = "c_at";
const columnUpdatedAt = "u_at";
const columnEmail = "email";
const columnAvatar = "avatar";
const columnName = "name";
const columnDetail = "detail";

// app specific
