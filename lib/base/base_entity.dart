
import 'constants.dart';

abstract class BaseEntity<K> {
  K get uniqueKey;

  BaseEntity.update();

  BaseEntity.fromMap(Map<String, dynamic> map) : this.update();

  Map<String, dynamic> toMap();

  /// Used for searching, It will check weather entity contains query
  bool search(String searchQuery);
}

abstract class BaseTitleDescriptionEntity {
  String get title;

  String get detail;
}

abstract class BaseAvatarEntity {
  String get avatar;
}

abstract class BaseDbEntity {
  String get createdBy;

  String get updatedBy;

  DateTime get created;

  DateTime get updated;
}

class Entity<K> implements BaseEntity<K> {
  K _id;

  Entity(this._id);

  Entity.update({id}) {
    this._id = id ?? this._id;
  }

  Entity.fromMap(Map<String, dynamic> map) : assert(map[columnId] != null) {
    _id = map[columnId];
  }

  void initWithMap(Map<String, dynamic> map){
    _id = map[columnId];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{columnId: _id};
    return map;
  }

  bool operator ==(o) => o is Entity && o.hashCode == this.hashCode;

  @override
  int get hashCode {
    return uniqueKey.hashCode ?? super.hashCode;
  }

  @override
  bool search(String searchQuery) {
    return false;
  }

  @override
  get uniqueKey => _id;
}

class DbEntity extends Entity implements BaseDbEntity {
  String _createdBy;
  String _updatedBy;
  DateTime _created = DateTime.now();
  DateTime _updated = DateTime.now();

  DbEntity(id, this._createdBy, this._updatedBy) : super(id);

  DbEntity.update(
      {id, info, String createdBy, String updatedBy, int created, int updated})
      : super.update(id: id) {
    this._createdBy = createdBy ?? this._createdBy;
    this._updatedBy = updatedBy ?? this._updatedBy;

    if (created != null) {
      this._created = DateTime.fromMillisecondsSinceEpoch(created);
    }

    if (updated != null) {
      this._updated = DateTime.fromMillisecondsSinceEpoch(updated);
    }
  }

  DbEntity.fromMap(Map<String, dynamic> map)
      : this.update(
            id: map[columnId],
            info: map[columnInfo],
            createdBy: map[columnCreatedBy],
            created: map[columnCreatedAt],
            updated: map[columnUpdatedAt],
            updatedBy: map[columnUpdatedBy]);

  @override
  Map<String, dynamic> toMap() {
    var map = super.toMap();
    map[columnCreatedBy] = createdBy;
    map[columnCreatedAt] = created.millisecondsSinceEpoch;
    map[columnUpdatedAt] = updated.millisecondsSinceEpoch;
    map[columnUpdatedBy] = updatedBy;
    return map;
  }

  String getDisplayUpdateTime() {
    if (updated != null) {
      return Constants.defaultDisplayDateFormat.format(updated);
    }
    return '';
  }

  String getDisplayCreatedTime() {
    if (created != null) {
      return Constants.defaultDisplayDateFormat.format(created);
    }
    return '';
  }

  @override
  get updatedBy => _updatedBy;

  @override
  get created => _created;

  @override
  get createdBy => _createdBy;

  @override
  get updated => _updated;
}
