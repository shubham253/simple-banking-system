import 'package:flutter/material.dart';
import 'package:flutter_app/screens/account_holder_detail_screen.dart';
import 'package:flutter_app/screens/transaction_history_screen.dart';
import 'app_specific_utils/app_constants.dart';
import 'app_specific_utils/app_events.dart';
import 'app_specific_utils/app_helper.dart';
import 'app_specific_utils/base_widgets.dart';
import 'app_specific_utils/event.dart';
import 'app_specific_utils/styles.dart';
import 'models/account_details_entity.dart';
import 'models/transaction_details_entity.dart';

class AccountDetailRowWidget extends EntityWidget<AccountDetailEntity> {
  AccountDetailRowWidget(
      AccountDetailEntity item, ValueChanged<AppEvent> callback)
      : super(item, callback: callback);

  @override
  _AccountDetailRowWidgetState createState() => _AccountDetailRowWidgetState();
}

class _AccountDetailRowWidgetState extends State<AccountDetailRowWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: Styles.cardViewStyle(radius: 4),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Account number: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    widget.item.accountNumber,
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Account type: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    widget.item.accountType,
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Account Holder Name: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    widget.item.primaryUser.fullName,
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Account Balance: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    '${widget.item.accountBalance.toString()}\$',
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Date: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    AppHelper.convertDateToString(
                        date: DateTime.fromMillisecondsSinceEpoch(
                            int.parse(widget.item.createdAt))),
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return AccountHolderDetailScreen(
                          accountType: widget.item.accountType,
                          account: widget.item);
                    }));
                  },
                  child: Text(
                    'Account holder details',
                    style: Styles.whiteTextStyle(13.0, false),
                  ),
                )),
                Container(
                  width: 6.0,
                ),
                Expanded(
                    child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return TransactionHistoryScreen(widget.item);
                    })).then((onValue) {
                      widget.callback(
                          AppEvent(AppEvents.refreshAccountList, onValue));
                    });
                  },
                  child: Text(
                    'Transaction history',
                    style: Styles.whiteTextStyle(13.0, false),
                  ),
                )),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class TransactionDetailRowWidget
    extends EntityWidget<TransactionDetailEntity> {
  TransactionDetailRowWidget(TransactionDetailEntity item) : super(item);

  @override
  _TransactionDetailRowWidgetState createState() =>
      _TransactionDetailRowWidgetState();
}

class _TransactionDetailRowWidgetState
    extends State<TransactionDetailRowWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: Styles.cardViewStyle(radius: 4),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Transaction id: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    widget.item.transactionId,
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Account Number: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    widget.item.targetAccount,
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Date: ',
                    style: Styles.blackTextStyle(16.0, true),
                    maxLines: 1,
                  ),
                  Text(
                    AppHelper.convertDateToString(
                        date: DateTime.fromMillisecondsSinceEpoch(
                            int.parse(widget.item.createdAt))),
                    style: Styles.greyTextBoldStyle(16.0),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Row(
                children: <Widget>[
                  getAmountWidget(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getAmountWidget() {
    return Text(
      '${widget.item.amount}\$  ${isAmountCredited() ? '+ credited' : '- debited'}',
      style: TextStyle(
          color: isAmountCredited() ? Colors.green : Colors.red,
          fontSize: 16.0,
          fontWeight: FontWeight.bold),
    );
  }

  bool isAmountCredited() {
    return widget.item.transactionType == AppConstants.transactionTypeCredited;
  }
}
