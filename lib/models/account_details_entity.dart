import 'package:flutter_app/models/transaction_details_entity.dart';
import 'package:flutter_app/models/user_details_entity.dart';

class AccountDetailEntity {
  String accountType;
  String accountNumber;
  double accountBalance;
  String createdAt;
  UserDetailsEntity primaryUser;
  UserDetailsEntity secondaryUser;
  List<TransactionDetailEntity> transactions;
  int lastInterestTimeSpan;


  AccountDetailEntity.empty({this.lastInterestTimeSpan = 0});

  AccountDetailEntity(
      {this.accountType,
      this.accountNumber,
      this.accountBalance,
      this.createdAt,
      this.primaryUser,
      this.secondaryUser,
      this.transactions,this.lastInterestTimeSpan});


  initAccountDetailEntity(AccountDetailEntity data){
    this.accountType = data.accountType;
    this.accountNumber = data.accountNumber;
    this.accountBalance = data.accountBalance;
    this.createdAt = data.createdAt;
    this.primaryUser = data.primaryUser;
    this.secondaryUser = data.secondaryUser;
    this.transactions = data.transactions;
    this.lastInterestTimeSpan = data.lastInterestTimeSpan;
  }


  AccountDetailEntity.fromJson(Map<String, dynamic> json) {
    this.createdAt = json['createdAt'];
    this.accountType = json['accountType'];
    this.accountBalance = json['accountBalance'];
    this.accountNumber = json['accountNumber'];
    if (json['lastInterestTimeSpan'] != null) {
      this.lastInterestTimeSpan = json['lastInterestTimeSpan'];
    }
    this.primaryUser = json['primaryUser'] != null
        ? UserDetailsEntity.fromJson(json['primaryUser'])
        : null;

    this.secondaryUser = json['secondaryUser'] != null
        ? UserDetailsEntity.fromJson(json['secondaryUser'])
        : null;
    if (json['transactions'] != null) {
      this.transactions = (json['transactions'] as List) != null
          ? (json['transactions'] as List)
              .map((i) => TransactionDetailEntity.fromJson(i))
              .toList()
          : null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['accountType'] = this.accountType;
    data['accountBalance'] = this.accountBalance;
    data['accountNumber'] = this.accountNumber;
    data['lastInterestTimeSpan'] = this.lastInterestTimeSpan;
    if (this.primaryUser != null) {
      data['primaryUser'] = this.primaryUser.toJson();
    }

    if (this.secondaryUser != null) {
      data['secondaryUser'] = this.secondaryUser.toJson();
    }

    data['transactions'] = this.transactions != null
        ? this.transactions.map((i) => i.toJson()).toList()
        : null;
    return data;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccountDetailEntity &&
          runtimeType == other.runtimeType &&
          accountNumber == other.accountNumber;

  @override
  int get hashCode => accountNumber.hashCode;
}
