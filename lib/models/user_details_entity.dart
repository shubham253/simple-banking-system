class UserDetailsEntity {
  String fullName;
  String phoneNumber;
  String dob;
  String address;

  UserDetailsEntity.empty(
      {this.fullName = "",
      this.phoneNumber = "",
      this.dob = "",
      this.address = ""});

  UserDetailsEntity.fromJson(Map<String, dynamic> json) {
    this.fullName = json['fullName'];
    this.phoneNumber = json['phoneNumber'];
    this.dob = json['dob'];
    this.address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fullName'] = this.fullName;
    data['phoneNumber'] = this.phoneNumber;
    data['dob'] = this.dob;
    data['address'] = this.address;
    return data;
  }
}
