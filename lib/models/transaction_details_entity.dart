class TransactionDetailEntity{

  String createdAt;
  double amount;
  String transactionType;
  String targetAccount;
  String transactionId;


  TransactionDetailEntity({this.transactionId,this.createdAt, this.amount, this.transactionType,
      this.targetAccount});

  TransactionDetailEntity.fromJson(Map<String, dynamic> json) {
    this.createdAt = json['createdAt'];
    this.amount = json['amount'];
    this.transactionType = json['transactionType'];
    this.targetAccount = json['targetAccount'];
    this.transactionId = json['transactionId'];
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['amount'] = this.amount;
    data['transactionType'] = this.transactionType;
    data['targetAccount'] = this.targetAccount;
    data['transactionId'] = this.transactionId;
    return data;
  }

}



