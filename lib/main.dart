import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/screens/account_holder_detail_screen.dart';
import 'package:flutter_app/screens/create_account_screen.dart';
import 'package:flutter_app/screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_specific_utils/app_constants.dart';
import 'app_specific_utils/app_helper.dart';
import 'app_specific_utils/current_session.dart';
import 'app_specific_utils/preference_helper.dart';
import 'models/transaction_details_entity.dart';

void main() {
  SharedPreferences.getInstance().then((p) {
    PreferenceHelper.init(p);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((onValue) {
      runApp(MyApp());
    });
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        AppConstants.homeScreen: (BuildContext context) => HomeScreen(),
        AppConstants.createAccountScreen: (BuildContext context) =>
            CreateAccountScreen(),
        AppConstants.accountHolderDetailScreen: (BuildContext context) =>
            AccountHolderDetailScreen(),
      },
      title: 'Simple Banking System',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: getNextScreen(),
    );
  }

  Widget getNextScreen() {
    calculateInterest();

    if (CurrentSession.i.getUserAccounts() == null) {
      return CreateAccountScreen();
    } else {
      return HomeScreen();
    }
  }

  void calculateInterest() {
    if (CurrentSession.i.getUserAccounts() == null ||
        CurrentSession.i.getUserAccounts().length == 0) {
      return;
    }

    for (int i = 0; i < CurrentSession.i.getUserAccounts().length; i++) {
      var totalInterestPercentage = ((DateTime.now().millisecondsSinceEpoch -
                      CurrentSession.i
                          .getUserAccounts()[i]
                          .lastInterestTimeSpan) /
                  AppConstants.interestPeriod)
              .floor() *
          AppConstants.interestPercentage;

      double amountWithInterest =
          (CurrentSession.i.getUserAccounts()[i].accountBalance *
                  totalInterestPercentage) /
              100;
      if (amountWithInterest > 0.0) {
        CurrentSession.i.executeTransaction(
            CurrentSession.i.getUserAccounts()[i],
            amountWithInterest,
            AppConstants.transactionTypeCredited,
            CurrentSession.i.getUserAccounts()[i].accountNumber);
      }
    }

    CurrentSession.i.saveDataLocally();
  }
}
