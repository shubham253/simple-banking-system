import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/app_specific_utils/app_constants.dart';
import 'package:flutter_app/app_specific_utils/app_helper.dart';
import 'package:flutter_app/app_specific_utils/current_session.dart';
import 'package:flutter_app/app_specific_utils/styles.dart';
import 'package:flutter_app/base/constants.dart';
import 'package:flutter_app/models/account_details_entity.dart';
import 'package:flutter_app/models/transaction_details_entity.dart';

import '../widgets.dart';

class TransactionHistoryScreen extends StatefulWidget {
  final AccountDetailEntity accountDetailsEntity;

  TransactionHistoryScreen(this.accountDetailsEntity);

  @override
  _TransactionHistoryScreenState createState() =>
      _TransactionHistoryScreenState();
}

class _TransactionHistoryScreenState extends State<TransactionHistoryScreen> {
  List<TransactionDetailEntity> transactions;
  bool isFundsTransferred = false;

  @override
  void initState() {
    super.initState();
    transactions = widget.accountDetailsEntity.transactions;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop(this.isFundsTransferred);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Transactions'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(Constants.mediumPadding),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Card(
                  shape: Styles.cardViewStyle(radius: 4.0),
                  child: Padding(
                    padding: EdgeInsets.all(Constants.mediumPadding),
                    child: Text(
                      'Current available balance = ${widget.accountDetailsEntity.accountBalance}\$',
                      style: Styles.blackTextStyle(18.0, true),
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: transactions.length,
                      itemBuilder: (BuildContext context, int index) {
                        return TransactionDetailRowWidget(transactions[index]);
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: Constants.largePadding),
                  child: RaisedButton(
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      showFundTransferDialog();
                    },
                    child: Text(
                      'Transfer Funds',
                      style: Styles.whiteTextStyle(13.0, false),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showFundTransferDialog() {
    var txtAccNoController = TextEditingController();
    var txtAmountController = TextEditingController();
    var formKey = GlobalKey<FormState>();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Transfer funds"),
          content: Container(
            height: 200.0,
            child: Form(
              key: formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: TextFormField( inputFormatters: [
                      LengthLimitingTextInputFormatter(10),
                    ],
                      controller: txtAccNoController,
                      style: Styles.textFormFieldStyle(),
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                      // ignore: missing_return
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter Account no';
                        }
                      },
                      decoration: InputDecoration(
                        hintText: "Account no*",
                        hintStyle: Styles.textFormFieldHintStyle(),
                        errorStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: TextFormField(
                      controller: txtAmountController,
                      style: Styles.textFormFieldStyle(),
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      // ignore: missing_return
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter amount';
                        } else if (widget.accountDetailsEntity.accountBalance <
                            double.parse(value)) {
                          return 'Insufficient funds';
                        }
                      },
                      decoration: InputDecoration(
                        hintText: "Amount*",
                        hintStyle: Styles.textFormFieldHintStyle(),
                        errorStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog

            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Transfer"),
              onPressed: () {
                if (formKey.currentState.validate()) {
                  setState(() {
                    double transferAmount =
                        double.parse(txtAmountController.text);

                    widget.accountDetailsEntity.initAccountDetailEntity(
                        CurrentSession.i.executeTransaction(
                            widget.accountDetailsEntity,
                            transferAmount,
                            AppConstants.transactionTypeDebited,
                            txtAccNoController.text));

                    AppHelper.showToast(
                        'funds transfered sucessfully.', context);

                    this.isFundsTransferred = true;

                    Navigator.of(context).pop();
                  });
                }
              },
            ),
          ],
        );
      },
    );
  }
}
