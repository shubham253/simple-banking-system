import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/app_specific_utils/app_events.dart';
import 'package:flutter_app/app_specific_utils/current_session.dart';
import 'package:flutter_app/app_specific_utils/styles.dart';
import 'package:flutter_app/base/constants.dart';
import 'package:flutter_app/models/account_details_entity.dart';

import '../widgets.dart';
import 'create_account_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<AccountDetailEntity> accounts;

  @override
  void initState() {
    super.initState();
    accounts = CurrentSession.i.getUserAccounts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bank Accounts'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(Constants.mediumPadding),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                  child: ListView.builder(
                      itemCount: accounts.length,
                      itemBuilder: (BuildContext context, int index) {
                        return AccountDetailRowWidget(accounts[index], (event) {
                          switch (event.type) {
                            case AppEvents.refreshAccountList:
                              setState(() {
                                accounts = CurrentSession.i.getUserAccounts();
                              });
                              break;
                          }
                        });
                      })),
              Padding(
                padding: const EdgeInsets.only(top: Constants.largePadding),
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CreateAccountScreen();
                    }));
                  },
                  child: Text(
                    'Add Account',
                    style: Styles.whiteTextStyle(13.0, false),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
