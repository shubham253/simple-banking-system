import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/app_specific_utils/app_constants.dart';
import 'package:flutter_app/app_specific_utils/app_helper.dart';
import 'package:flutter_app/app_specific_utils/current_session.dart';
import 'package:flutter_app/app_specific_utils/styles.dart';
import 'package:flutter_app/base/constants.dart';
import 'package:flutter_app/models/account_details_entity.dart';
import 'package:flutter_app/models/transaction_details_entity.dart';
import 'package:flutter_app/models/user_details_entity.dart';
import 'package:intl/intl.dart';

class AccountHolderDetailScreen extends StatefulWidget {
  final String accountType;
  final AccountDetailEntity account;

  AccountHolderDetailScreen(
      {@required this.accountType, @required this.account});

  @override
  _AccountHolderDetailScreenState createState() =>
      _AccountHolderDetailScreenState();
}

class _AccountHolderDetailScreenState extends State<AccountHolderDetailScreen> {
  var _formKeyPrimaryAccountHolder = GlobalKey<FormState>();
  var _formKeySecondaryAccountHolder = GlobalKey<FormState>();

  DateTime selectedDate = DateTime.now();

  var _txtFullNameControllerPrimary = TextEditingController();
  var _txtPhoneNumberControllerPrimary = TextEditingController();
  var _txtDOBControllerPrimary = TextEditingController();
  var _txtAddressControllerPrimary = TextEditingController();

  var _txtFullNameControllerSecondary = TextEditingController();
  var _txtPhoneNumberControllerSecondary = TextEditingController();
  var _txtDOBControllerSecondary = TextEditingController();
  var _txtAddressControllerSecondary = TextEditingController();

  UserDetailsEntity primaryUser;
  UserDetailsEntity secondaryUser;

  @override
  void initState() {
    super.initState();
    primaryUser = (widget.account != null && widget.account.primaryUser != null)
        ? widget.account.primaryUser
        : UserDetailsEntity.empty();
    secondaryUser =
        (widget.account != null && widget.account.secondaryUser != null)
            ? widget.account.secondaryUser
            : UserDetailsEntity.empty();

    if (primaryUser != null) {
      _txtFullNameControllerPrimary.text = primaryUser.fullName;
      _txtPhoneNumberControllerPrimary.text = primaryUser.phoneNumber;
      _txtDOBControllerPrimary.text = primaryUser.dob;
      _txtAddressControllerPrimary.text = primaryUser.address;
    }

    if (secondaryUser != null) {
      _txtFullNameControllerSecondary.text = secondaryUser.fullName;
      _txtPhoneNumberControllerSecondary.text = secondaryUser.phoneNumber;
      _txtDOBControllerSecondary.text = secondaryUser.dob;
      _txtAddressControllerSecondary.text = secondaryUser.address;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account holder Details'),
      ),
      body: Padding(
        padding: EdgeInsets.all(Constants.largePadding),
        child: Container(
          child: ListView(
            children: <Widget>[
              Form(
                key: _formKeyPrimaryAccountHolder,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Primary account holder details',
                      style: Styles.blackTextStyle(14.0, true),
                      textAlign: TextAlign.left,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        enabled: isTextFieldEnabled(),
                        controller: _txtFullNameControllerPrimary,
                        style: Styles.textFormFieldStyle(),
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter full name';
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Full Name*",
                          hintStyle: Styles.textFormFieldHintStyle(),
                          errorStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        enabled: isTextFieldEnabled(),
                        controller: _txtPhoneNumberControllerPrimary,
                        style: Styles.textFormFieldStyle(),
                        keyboardType: TextInputType.phone,
                        textInputAction: TextInputAction.next,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(10),
                        ],
                        
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter phone number';
                          } else if (value.length < 10) {
                            return 'Invalid phone number';
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Phone Number*",
                          hintStyle: Styles.textFormFieldHintStyle(),
                          errorStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (!isTextFieldEnabled()) {
                          return;
                        }
                        _selectDate(context).then((onValue) {
                          if (onValue != null) {
                            setState(() {
                              DateFormat format =
                                  Constants.defaultDisplayDateFormat;
                              _txtDOBControllerPrimary.text =
                                  '${format.format(onValue)}';
                            });
                          }
                        });
                      },
                      child: IgnorePointer(
                        ignoring: true,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: TextFormField(
                            enabled: isTextFieldEnabled(),
                            controller: _txtDOBControllerPrimary,
                            style: Styles.textFormFieldStyle(),
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter date of birth';
                              }
                            },
                            decoration: InputDecoration(
                              hintText: "DOB*",
                              hintStyle: Styles.textFormFieldHintStyle(),
                              errorStyle: TextStyle(
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        enabled: isTextFieldEnabled(),
                        controller: _txtAddressControllerPrimary,
                        style: Styles.textFormFieldStyle(),
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Please enter address';
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Address*",
                          hintStyle: Styles.textFormFieldHintStyle(),
                          errorStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              widget.accountType == AppConstants.accountTypeJoint
                  ? Form(
                      key: _formKeySecondaryAccountHolder,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: Constants.largePadding),
                            child: Text(
                              'Secondary account holder details',
                              style: Styles.blackTextStyle(14.0, true),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              enabled: isTextFieldEnabled(),
                              controller: _txtFullNameControllerSecondary,
                              style: Styles.textFormFieldStyle(),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter full name';
                                }
                              },
                              decoration: InputDecoration(
                                hintText: "Full Name*",
                                hintStyle: Styles.textFormFieldHintStyle(),
                                errorStyle: TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              enabled: isTextFieldEnabled(),
                              controller: _txtPhoneNumberControllerSecondary,
                              style: Styles.textFormFieldStyle(),
                              keyboardType: TextInputType.phone,
                              textInputAction: TextInputAction.next,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(10),
                              ],
                              
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter phone number';
                                } else if (value.length < 10) {
                                  return 'Invalid phone number';
                                }
                              },
                              decoration: InputDecoration(
                                hintText: "Phone Number*",
                                hintStyle: Styles.textFormFieldHintStyle(),
                                errorStyle: TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              if (!isTextFieldEnabled()) {
                                return;
                              }
                              _selectDate(context).then((onValue) {
                                if (onValue != null) {
                                  setState(() {
                                    DateFormat format =
                                        Constants.defaultDisplayDateFormat;
                                    _txtDOBControllerSecondary.text =
                                        '${format.format(onValue)}';
                                  });
                                }
                              });
                            },
                            child: IgnorePointer(
                              ignoring: true,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: TextFormField(
                                  enabled: isTextFieldEnabled(),
                                  controller: _txtDOBControllerSecondary,
                                  style: Styles.textFormFieldStyle(),
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter date of birth';
                                    }
                                  },
                                  decoration: InputDecoration(
                                    hintText: "DOB*",
                                    hintStyle: Styles.textFormFieldHintStyle(),
                                    errorStyle: TextStyle(
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              enabled: isTextFieldEnabled(),
                              controller: _txtAddressControllerSecondary,
                              style: Styles.textFormFieldStyle(),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,

                              
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter address';
                                }
                              },
                              decoration: InputDecoration(
                                hintText: "Address*",
                                hintStyle: Styles.textFormFieldHintStyle(),
                                errorStyle: TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              isTextFieldEnabled()
                  ? Padding(
                      padding:
                          const EdgeInsets.only(top: Constants.largePadding),
                      child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          if (isValidInput()) {
                            initCreatingAccountProcess();
                            Navigator.pushNamedAndRemoveUntil(
                                context, AppConstants.homeScreen, (r) => false);
                          }
                        },
                        child: Text(
                          'Create Account',
                          style: Styles.whiteTextStyle(13.0, false),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  bool isValidInput() {
    return _formKeyPrimaryAccountHolder.currentState.validate() &&
        isValidSecondaryUserDetails();
  }

  Future<DateTime> _selectDate(BuildContext context) async {
    return await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1970, 1),
        lastDate: DateTime.now());
  }

  void initCreatingAccountProcess() {
    //creating primary user object
    primaryUser.fullName = _txtFullNameControllerPrimary.text;
    primaryUser.phoneNumber = _txtPhoneNumberControllerPrimary.text;
    primaryUser.dob = _txtDOBControllerPrimary.text;
    primaryUser.address = _txtAddressControllerPrimary.text;

    //creating secondary user object if required
    if (widget.accountType == AppConstants.accountTypeJoint) {
      secondaryUser.fullName = _txtFullNameControllerSecondary.text;
      secondaryUser.phoneNumber = _txtPhoneNumberControllerSecondary.text;
      secondaryUser.dob = _txtDOBControllerSecondary.text;
      secondaryUser.address = _txtAddressControllerSecondary.text;
    }

    //generating random account number
    String accountNumber = AppHelper.getRandomNumber();

    // creating first transaction
    TransactionDetailEntity transaction = TransactionDetailEntity(
        transactionId: AppHelper.getRandomNumber(),
        createdAt: DateTime.now().millisecondsSinceEpoch.toString(),
        amount: AppConstants.initialAccountBalance,
        transactionType: AppConstants.transactionTypeCredited,
        targetAccount: accountNumber);

    List<TransactionDetailEntity> transactions = List();
    transactions.add(transaction);

    //creating new account
    AccountDetailEntity account = AccountDetailEntity(
        accountNumber: accountNumber,
        accountType: widget.accountType,
        accountBalance: AppConstants.initialAccountBalance,
        primaryUser: primaryUser,
        secondaryUser: secondaryUser,
        createdAt: DateTime.now().millisecondsSinceEpoch.toString(),
        transactions: transactions,
        lastInterestTimeSpan: DateTime.now().millisecondsSinceEpoch);

    //adding account to local data
    if (CurrentSession.i.localData.accounts == null) {
      CurrentSession.i.localData.accounts = List();
    }
    CurrentSession.i.localData.accounts.add(account);
    CurrentSession.i.saveDataLocally();
  }

  bool isTextFieldEnabled() {
    return widget.account == null;
  }

  bool isValidSecondaryUserDetails() {
    if (widget.accountType == AppConstants.accountTypeJoint) {
      return _formKeySecondaryAccountHolder.currentState.validate();
    } else {
      return true;
    }
  }
}
