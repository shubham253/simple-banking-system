import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/app_specific_utils/app_constants.dart';
import 'package:flutter_app/app_specific_utils/styles.dart';
import 'package:flutter_app/base/constants.dart';

import 'account_holder_detail_screen.dart';

class CreateAccountScreen extends StatefulWidget {
  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  String _selectedAccountType;

  @override
  void initState() {
    super.initState();
    _selectedAccountType = AppConstants.accountTypeSingle;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Create New Account')),
      body: Padding(
        padding: const EdgeInsets.all(Constants.largePadding),
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: Constants.largeMargin),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Select your account type',
                  style: Styles.blackTextStyle(20.0, false),
                ),
              ),
              Row(
                children: <Widget>[
                  Radio(
                    value: AppConstants.accountTypeSingle,
                    groupValue: _selectedAccountType,
                    onChanged: (String value) {
                      setState(() {
                        _selectedAccountType = value;
                      });
                    },
                  ),
                  Text(
                    'Single',
                    style: Styles.greyTextStyle(14.0),
                  ),
                  Radio(
                    value: AppConstants.accountTypeJoint,
                    groupValue: _selectedAccountType,
                    onChanged: (String value) {
                      setState(() {
                        _selectedAccountType = value;
                      });
                    },
                  ),
                  Text(
                    'Joint',
                    style: Styles.greyTextStyle(14.0),
                  ),
                ],
              ),
              RaisedButton(color: Theme.of(context).primaryColor,
                onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder:(context){
                  return  AccountHolderDetailScreen(accountType: _selectedAccountType,);
                }));
                },
                child: Text(
                  'Next',
                  style: Styles.whiteTextStyle(13.0, false),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
