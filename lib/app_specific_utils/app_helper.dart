import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter_app/base/constants.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class AppHelper {

  static showToast(String message, BuildContext context, [int length = 1]) {
    Toast.show(message, context, duration: length, gravity: Toast.BOTTOM);
  }

  static DateFormat getDefaultDateFormat() {
    return Constants.defaultDisplayDateFormat;
  }

  static String convertDateToString({DateFormat dateFormat, DateTime date}) {
    if (dateFormat == null) {
      dateFormat = getDefaultDateFormat();
    }
    return '${dateFormat.format(date)}';
  }

  static String getRandomNumber() {
    var rnd = new Random();
    var next = rnd.nextDouble() * 10000000000;
    while (next < 100000000000) {
      if (next.round().toString().length > 9) {
        return next.round().toString();
      }
    }
  }
}
