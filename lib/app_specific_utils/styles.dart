import 'package:flutter/material.dart';

class Styles {
  static TextStyle textFormFieldStyle() {
    return TextStyle(fontSize: 14.0);
  }

  static TextStyle textFormFieldHintStyle() {
    return TextStyle(color: Color.fromARGB(255, 180, 180, 180),);
  }

  static TextStyle greyTextStyle(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: Colors.grey,
      );

  static TextStyle greyTextBoldStyle(double fontSize) => TextStyle(
      color: Colors.grey,
      fontSize: fontSize,
      fontWeight: FontWeight.bold);



  static TextStyle blackTextStyle(double fontSize, bool isBold) {
    return TextStyle(
        color: Colors.black,
        fontSize: fontSize,
        fontWeight: isBold ? FontWeight.bold : FontWeight.normal);
  }



  static TextStyle whiteTextStyle(double fontSize, bool isBold) {
    return TextStyle(
        color: Colors.white,
        fontSize: fontSize,
        fontWeight: isBold ? FontWeight.bold : FontWeight.normal);
  }

  static RoundedRectangleBorder cardViewStyle({double radius = 8.0}) {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(radius)));
  }
}
