import 'package:flutter_app/models/account_details_entity.dart';
import 'package:flutter_app/models/transaction_details_entity.dart';

import 'app_constants.dart';
import 'app_helper.dart';
import 'base_current_session.dart';
import 'local_data.dart';

class CurrentSession extends BaseCurrentSession<LocalData> {
  static final CurrentSession _currentSession = CurrentSession._internal();

  CurrentSession(LocalData localData) : super();

  CurrentSession._internal() : super();

  @override
  void initLocalData(Map<String, dynamic> data) {
    localData = LocalData.fromJson(data);
  }

  static CurrentSession get i => _currentSession;

  @override
  void saveDataLocally() {
    // TODO: implement saveDataLocally
    super.saveDataLocally();
  }

  List<AccountDetailEntity> getUserAccounts() {
    return localData.accounts;
  }

 AccountDetailEntity executeTransaction(
      AccountDetailEntity userAccount, double amount, String transactionType,String targetAccountNumber) {

    //creating transaction
    TransactionDetailEntity transaction = TransactionDetailEntity(
        transactionId: AppHelper.getRandomNumber(),
        createdAt: DateTime.now().millisecondsSinceEpoch.toString(),
        amount: amount,
        transactionType: transactionType,
        targetAccount: targetAccountNumber);

    //adding transaction to account
    userAccount.transactions.add(transaction);

    //calculating current account balance
    double currentAccountBalance =
        (transactionType == AppConstants.transactionTypeCredited)
            ? userAccount.accountBalance + amount
            : userAccount.accountBalance - amount;

    userAccount.accountBalance = currentAccountBalance;

    userAccount.lastInterestTimeSpan = DateTime.now().millisecondsSinceEpoch;

    //refreshing account details
    getUserAccounts()[getUserAccounts().indexOf(userAccount)].initAccountDetailEntity(userAccount);

    saveDataLocally();

    return getUserAccounts()[getUserAccounts().indexOf(userAccount)];
  }
}
