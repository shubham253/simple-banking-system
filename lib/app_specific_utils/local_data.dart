import 'package:flutter_app/base/base_entity.dart';
import 'package:flutter_app/models/account_details_entity.dart';

class LocalData extends BaseEntity {
  List<AccountDetailEntity> accounts;
  LocalData.init() : super.update();

  LocalData.fromJson(Map<String, dynamic> json) : super.fromMap(json) {
    if (json['accounts'] != null) {
      this.accounts = (json['accounts'] as List) != null
          ? (json['accounts'] as List)
              .map((i) => AccountDetailEntity.fromJson(i))
              .toList()
          : null;
    }
  }

  @override
  bool search(String searchQuery) {
    // TODO: implement search
    return null;
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accounts'] = this.accounts != null
        ? this.accounts.map((i) => i.toJson()).toList()
        : null;
    return data;
  }

  @override
  // TODO: implement uniqueKey
  get uniqueKey => null;
}
