
class AppConstants {

  //account types
  static final String accountTypeSingle = "Single Account";
  static final String accountTypeJoint = "Joint Account";

  // static app values
  static const double initialAccountBalance = 5000.0;

  // transaction types
  static final String transactionTypeCredited = "credited";
  static final String transactionTypeDebited = "debited";

  // screens
  static final String homeScreen = "/homeScreen";
  static final String createAccountScreen = "/createAccountScreen";
  static final String accountHolderDetailScreen = "/accountHolderDetailScreen";
  static final String transactionHistoryScreen = "/transactionHistoryScreen";

  //interest duration
  static final int interestPeriod = 3600000;
  static final int interestPercentage =5;

}



