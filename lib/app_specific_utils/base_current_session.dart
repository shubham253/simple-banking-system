import 'dart:convert';

import 'package:flutter_app/app_specific_utils/preference_helper.dart';
import 'package:flutter_app/base/base_entity.dart';

import 'local_data.dart';

abstract class BaseCurrentSession<T extends BaseEntity> {
  T localData;
  int _keyLocalData = 99;

  BaseCurrentSession() {
    String data = PreferenceHelper.i.readString(_keyLocalData);
    if (data.isEmpty || data == null) {
      localData = new LocalData.init() as T;
    } else {
      initLocalData(json.decode(data));
    }
  }

  void initLocalData(Map<String, dynamic> data);

  void saveDataLocally() {
    final Map<String, dynamic> data = localData.toMap();

    String encodedData = json.encode(data);

    PreferenceHelper.i.putString(_keyLocalData, encodedData);
  }

  void clearLocalData() {
    this.localData = LocalData.init() as T;
    saveDataLocally();
  }
}
