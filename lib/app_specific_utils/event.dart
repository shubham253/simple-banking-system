
class AppEvent<T>{
  String type;
  T data;
  AppEvent(this.type, this.data);
}
