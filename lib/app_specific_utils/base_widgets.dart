import 'package:flutter/cupertino.dart';

import 'event.dart';

abstract class EntityWidget<T> extends StatefulWidget {
  final T item;
  final ValueChanged<AppEvent> callback;

  EntityWidget(this.item, {this.callback, Key key}) : super(key: key);
}