// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_app/app_specific_utils/app_constants.dart';
import 'package:flutter_app/app_specific_utils/app_helper.dart';
import 'package:flutter_app/models/account_details_entity.dart';
import 'package:flutter_app/models/transaction_details_entity.dart';
import 'package:flutter_app/models/user_details_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_app/main.dart';

void main() {

  /// tests related to entity
  group("AccountDetailEntity", () {

    UserDetailsEntity primaryUser = UserDetailsEntity.empty();
    primaryUser.fullName = 'Test Name 1';
    primaryUser.phoneNumber = '9999999999';
    primaryUser.dob = 'test DOB';
    primaryUser.address = 'Test address';

    //generating random account number
    String accountNumber = AppHelper.getRandomNumber();

    // creating first transaction
    TransactionDetailEntity transaction = TransactionDetailEntity(
        transactionId: AppHelper.getRandomNumber(),
        createdAt: DateTime.now().millisecondsSinceEpoch.toString(),
        amount: AppConstants.initialAccountBalance,
        transactionType: AppConstants.transactionTypeCredited,
        targetAccount: accountNumber);

    List<TransactionDetailEntity> transactions = List();
    transactions.add(transaction);

    //creating new account
    AccountDetailEntity account = AccountDetailEntity(
        accountNumber: accountNumber,
        accountType: AppConstants.accountTypeSingle,
        accountBalance: AppConstants.initialAccountBalance,
        primaryUser: primaryUser,
        createdAt: DateTime.now().millisecondsSinceEpoch.toString(),
        transactions: transactions,
        lastInterestTimeSpan: DateTime.now().millisecondsSinceEpoch);


    test('Create - empty AccountDetailEntity with zero lastInterestTimeSpan should be created', () {
       expect(AccountDetailEntity.empty().lastInterestTimeSpan, 0);
    });

    test('Create - AccountDetailEntity accountNumber should be initialised', () {

       expect(account.accountNumber,isNot(null));

    });

    test('Create - AccountDetailEntity accountType should be initialised', () {

       expect(account.accountType,isNot(null));

    });

    test('Create - AccountDetailEntity accountBalance should be initialised', () {

       expect(account.accountBalance,isNot(null));

    });

    test('Create - AccountDetailEntity primaryUser should be initialised', () {

       expect(account.primaryUser,isNot(null));

    });

    test('Create - AccountDetailEntity transactions should be initialised', () {

       expect(account.transactions,isNot(null));

    });

    test('Create - AccountDetailEntity lastInterestTimeSpan should be initialised', () {

       expect(account.lastInterestTimeSpan,isNot(null));

    });
    
  });



}
